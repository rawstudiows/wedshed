import React from "react";
import PropTypes from "prop-types";
import Head from "next/head"; import { ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from "../utils/theme";
import Nav from "../components/Nav"
import Footer from "../components/Footer"

export default function Layout ()
{
    return (
        <div>
            <Nav />
            <Component {...pageProps} />
            <Footer />
        </div>
    )
}

