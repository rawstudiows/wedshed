import Link from 'next/link';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles( ( theme ) => ( {
    root: {
        display: 'flex',
        gap: '10px',
        fontWeight: 'bold',
    },

} ) );
export default function Nav ()
{
    const classes = useStyles();
    return (
        <div>
            <h1>Cool Nav coming soon!!</h1>
            <div className={classes.root}>
                <Link href="/">Home</Link><br />
                <Link href="/blog">Blog</Link>
            </div>
        </div>
    )
}
