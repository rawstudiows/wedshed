import { ApolloClient, InMemoryCache, gql } from "@apollo/client";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles( ( theme ) => ( {
    root: {
        flexGrow: 1,
    },
    paper: {
        height: 140,
        width: 100,
    },
    control: {
        padding: theme.spacing( 2 ),
    },
} ) );

export default function BlogList ( props )
{
    console.log( props )
    const classes = useStyles();

    return (
        <Grid container className={classes.root} spacing={2}>
            <Grid item xs={12}>
                <Grid container justifyContent="center" spacing={2}>
                    {[ 0, 1, 2 ].map( ( blog ) => (
                        <Grid key={blog} item>
                            <Paper className={classes.paper} />
                        </Grid>
                    ) )}
                </Grid>
            </Grid>
        </Grid >
    );
}

export async function getStaticProps ()
{
    const client = new ApolloClient( {
        uri: process.env.APP_URI,
        cache: new InMemoryCache(),
    } );

    const { data } = await client.query( {
        query: gql`
      query ARTICLE_QUERY {
        blogArticles {
          title
          slug
          ArticleBase {
            content
            link
            images {
              url
            }
          }
        }
      }
    `,
    } );

    return {
        props: {
            blogs: data.blogArticles,
        },
    };
}