import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";
import { withApollo } from "next-with-apollo";
import fetch from "isomorphic-unfetch";

const GRAPH_API_URL = "http://localhost:1337";

const link = new HttpLink( {
  fetch,
  uri: `${ GRAPH_API_URL }/graphql`,
} );

export default withApollo(
  ( initState ) =>
    new ApolloClient( {
      link: link,
      cache: new InMemoryCache().restore( initState || {} ),
    } )
);
