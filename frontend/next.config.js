const path = require( 'path' );
module.exports = {
  reactStrictMode: true,
  eslint: {
    // Warning: Dangerously allow production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  webpack: config =>
  {
    config.resolve.alias[ 'component' ] = path.join( __dirname, 'components' )
    config.resolve.alias[ 'public' ] = path.join( __dirname, 'public' )
    return config
  }
}
