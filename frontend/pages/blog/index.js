import { gql } from "@apollo/client";
import Query from "../../components/Query.components";
import BlogList from "../../components/BlogList";
import ARTICLE_QUERY from "../../apollo/ArticleQuery";


export default function blog ( { blogs } )
{
  console.log( blogs );
  return (
    <div className="">
      <BlogList />
      <Query slug query={ARTICLE_QUERY( "blog" )}>
        {( { data } ) =>
        {
          return data.blogArticles.map( ( blog ) =>
          {
            return <div key={blog.slug}>{blog.title}</div>;
          } );
        }}
      </Query>
    </div>
  );
}
